import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-assignment',
  templateUrl: './directives-assignment.component.html',
  styles: [`
    .whitecolor {
      color: white;
    }
  `]
})
export class DirectivesAssignmentComponent implements OnInit {
  messageVisible = false;
  clicks = [];

  constructor() { }

  ngOnInit() {

  }

  toggleMessage() {

    this.messageVisible = this.messageVisible === true ? false : true;

    if (this.messageVisible) {
      this.clicks.push({
        message: 'The truth was SHOWN!',
        counter: this.clicks.length + 1
      });
    } else {
      this.clicks.push({
        message: 'The truth was HIDDEN!',
        counter: this.clicks.length + 1
      });
    }

  }

  getBackgroundColor(counter) {
    if (counter >= 5) {
      return { backgroundColor: 'blue' };
    } else {
      return { backgroundColor: '' };
    }
  }
}
