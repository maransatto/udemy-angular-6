import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component.';
import { ServersComponent } from './servers/servers.component';
import { SuccessAlertComponent } from './alerts/success/success-alert.component';
import { DangerAlertComponent } from './alerts/danger/danger-alert.component';
import { WarningAlertComponent } from './alerts/warning/warning-alert.component';
import { SecondAssignmentComponent } from './assignments/second-assignment.component';
import { DirectivesAssignmentComponent } from './assignments/directives-assignment.component';

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    ServersComponent,
    SuccessAlertComponent,
    DangerAlertComponent,
    WarningAlertComponent,
    SecondAssignmentComponent,
    DirectivesAssignmentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
